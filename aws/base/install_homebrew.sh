#!/bin/bash

set +x

/bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"

(echo; echo 'eval "$(/home/linuxbrew/.linuxbrew/bin/brew shellenv)"') >> /root/.profile
eval "$(/home/linuxbrew/.linuxbrew/bin/brew shellenv)"

apt-get install -y build-essential
brew install gcc

brew install terraform terragrunt

terraform version
terragrunt --version
